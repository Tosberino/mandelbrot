#include <iostream>

using namespace std;

int main (){

    double x = 0;
    double y = 0;
    int i = 0;
    int maxi = 255;

    for(double y0 = -1.5; y0 < 1.5; y0 += 0.05){
        for(double x0 = -2.0; x0 < 2.0; x0 += 0.02 ){
            x = 0;
            y = 0;
            i = 0;
            while(x * x + y * y <= (2 * 2) && i < maxi){
                double xtemp = x * x - y * y + x0;
                y = 2 * x * y + y0;
                x = xtemp;
                i++;
            }

            if(i != maxi){
                cout << "#";
            } else {
                cout << " ";
            }
        }

        cout << endl;
    }


    return 0;
}
